/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package kog

import (
	"testing"

	pwe "gitlab.com/f5-pwe/common/v2"
)

type TestHook struct {
	Fired bool
}

func (c *TestHook) FireNotification(job *pwe.Job) error {
	c.Fired = true
	return nil
}

func TestHookFire(t *testing.T) {
	hook := &TestHook{}
	hooks := &NotifyHooks{}
	hooks.AddNotification(hook)

	job := &pwe.Job{}
	err := hooks.FireNotification(job)

	if err != nil {
		t.Errorf("test hook fire error: %v", err)
	}
	if !hook.Fired {
		t.Error("test hook did not fire")
	}
}
