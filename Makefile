ifndef GOPATH
$(warning You need to set up a GOPATH. Run "go help gopath".)
endif


PKG_NAME?=kog
DOCKER_HOST?=tcp://localhost:2375
SEMVER?=v0.0.0
run_id:=$(shell date -u '+%Y%m%d-%H%M%S')
MAIN=./cmd/$(PKG_NAME)/...

git:=$(strip $(shell which git 2> /dev/null))

ifeq ($(release),)
ifeq ($(git),)
	release:=$(strip $(shell date +%s))
else
	release:=$(strip $(shell $(git) log -1 --pretty=format:%ct))
endif
endif

CI_COMMIT_SHORT_SHA?=$(release)
GOOS?=linux
GOARCH?=amd64

.PHONY: build run test deps lin32 lin64 win32 win64 linux windows dar64 darwin docker all push hello-world

all: build

init: githooks
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(shell go env GOPATH)/bin $(GOLANGCI_LINT_VERSION)

githooks:
	git config core.hooksPath .githooks

clean:
	rm -rf bin

build:
	CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) go build -mod=vendor \
		-v -tags 'netgo osusergo' \
		-ldflags '-extldflags "-static" -X main.pkgName=$(PKG_NAME) -X main.version=$(SEMVER) -X main.commit=$(CI_COMMIT_SHORT_SHA)' \
		-o ./bin/$(PKG_NAME)-$(GOOS)-$(GOARCH)$(BIN_EXTENSION) $(MAIN)

lin32:
	GOOS=linux GOARCH=386 $(MAKE) build

lin64:
	GOOS=linux GOARCH=amd64 $(MAKE) build
	GOOS=linux GOARCH=arm64 $(MAKE) build

linux: lin32 lin64

win32:
	GOOS=windows GOARCH=386 BIN_EXTENSION=.exe $(MAKE) build

win64:
	GOOS=windows GOARCH=amd64 BIN_EXTENSION=.exe $(MAKE) build

windows: win32 win64

dar64:
	GOOS=darwin GOARCH=amd64 $(MAKE) build
	GOOS=darwin GOARCH=arm64 $(MAKE) build

darwin: dar64

run: build
	./bin/$(PKG_NAME)-$(GOOS)-$(GOARCH)$(BIN_EXTENSION) version

lint:
	golangci-lint run -v

format:
	go fmt ./...

test:
	git config --global http.sslverify "false"
	go test -mod=vendor ./... -tags 'release netgo osusergo' -v -coverprofile=coverage.out
	go tool cover -func=coverage.out

deps:
	go get -u -t ./...
	go mod tidy -compat=1.22
	go mod verify
	go mod vendor