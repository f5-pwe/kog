package kog

import (
	"encoding/json"
	"fmt"

	pwe "gitlab.com/f5-pwe/common/v2"
)

// consoleHook prints the body to console, useful for debugging
type consoleHook struct{}

func NewConsoleHook() NotifyHook {
	return &consoleHook{}
}

func (c *consoleHook) FireNotification(job *pwe.Job) error {
	body, err := json.Marshal(job)
	if err != nil {
		return err
	}
	fmt.Println(string(body))
	return nil
}
