package workflow

import (
	"gitlab.com/f5-pwe/common/v2/duration"
)

type Step struct {
	Name        string             `json:"name" yaml:"name"`
	ActionName  string             `json:"action" yaml:"action"`
	SuccessStep string             `json:"success,omitempty" yaml:"success,omitempty"`
	FailStep    string             `json:"fail,omitempty" yaml:"fail,omitempty"`
	AbortStep   string             `json:"abort,omitempty" yaml:"abort,omitempty"`
	TimeoutStep string             `json:"timeout,omitempty" yaml:"timeout,omitempty"`
	Timeout     *duration.Duration `json:"duration,omitempty" yaml:"duration,omitempty"`

	// Depreciated: use duration instead
	// TimeoutSeconds the number of seconds for the timeout
	TimeoutSeconds float64 `json:"timeout_seconds,omitempty" yaml:"timeout_seconds,omitempty"`
}

var FailedStep = &Step{}
