/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pwe

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/f5-pwe/common/v2/actions"
	"gitlab.com/f5-pwe/common/v2/variables"
	"gitlab.com/f5-pwe/common/v2/workflow"
)

type Job struct {
	ID              RequestID          `json:"correlation_id" yaml:"correlation_id"`
	ExecutionID     RequestID          `json:"run_id" yaml:"run_id"`
	Name            string             `json:"name" yaml:"name"`
	Description     string             `json:"description" yaml:"description"`
	CreatedDate     *time.Time         `json:"creation_date" yaml:"creation_date"`
	StartDate       *time.Time         `json:"start_date,omitempty" yaml:"start_date,omitempty"`
	UpdatedDate     *time.Time         `json:"updated_date,omitempty" yaml:"updated_date,omitempty"`
	EndDate         *time.Time         `json:"end_date,omitempty" yaml:"end_date,omitempty"`
	CurrentWorkflow string             `json:"current_workflow" yaml:"current_workflow"`
	Workflows       workflow.Workflows `json:"workflows,omitempty" yaml:"workflows,omitempty"`
	Actions         actions.Actions    `json:"actions,omitempty" yaml:"actions,omitempty"`
	Context         *variables.Context `json:"context,omitempty" yaml:"context,omitempty"`
	Status          JobStatus          `json:"status" yaml:"status"`
	CurrentStep     *string            `json:"current_step,omitempty" yaml:"current_step,omitempty"`
	StopOnFailure   bool               `json:"stop_on_failure,omitempty" yaml:"stop_on_failure,omitempty"`
}
type tmpJ Job

// UnmarshalJSON decodes the job, and validates the data
func (j *Job) UnmarshalJSON(b []byte) (err error) {
	var newJ tmpJ
	err = json.Unmarshal(b, &newJ)
	if err != nil {
		return
	}
	tmp := Job(newJ)
	err = tmp.Validate()
	*j = Job(tmp)
	return
}

// Completed return true if the job is in a completed status
func (j *Job) Completed() bool {
	return (j.Status == JOB_UNDER_INVESTIGATION ||
		j.Status == JOB_FAILURE ||
		j.Status == JOB_SUCCESS ||
		j.Status == JOB_CANCELED)
}

// Failed returns true is the job is in a failed status
func (j *Job) Failed() bool {
	return (j.Status == JOB_UNDER_INVESTIGATION ||
		j.Status == JOB_FAILURE)
}

// Pending return true if the job is in a pending status
func (j *Job) Pending() bool {
	return (j.Status == JOB_QUEUED ||
		j.Status == JOB_PENDING_RESOURCES)
}

func (j *Job) Workflow() (wf *workflow.Workflow) {
	wf, _ = j.Workflows[j.CurrentWorkflow]

	return
}

func (j *Job) Validate() (err error) {
	if j.ID == "0" || j.ID == "" {
		j.ID = NewRequestID()
	}
	if j.ExecutionID == "0" || j.ExecutionID == "" {
		j.ExecutionID = NewRequestID()
	}
	if nano, err := j.ID.UnixNano(); err == nil {
		cd := time.Unix(0, nano)
		j.CreatedDate = &cd
	}

	if j.CurrentWorkflow == "" {
		j.CurrentWorkflow = "run"
	}

	if j.Workflows == nil {
		return fmt.Errorf("Workflows is required")
	}

	if len(j.Workflows) == 0 {
		return fmt.Errorf("Atleast one workflow is required")
	}

	if _, ok := j.Workflows[j.CurrentWorkflow]; !ok {
		return fmt.Errorf("Workflow: %s does not exist", j.CurrentWorkflow)
	}

	if j.Actions == nil {
		return fmt.Errorf("Actions are required")
	}

	if j.Context == nil {
		j.Context = &variables.Context{}
	}

	if j.CurrentStep == nil {
		j.CurrentStep = String2Pointer(fmt.Sprintf("job:%s", j.Status.String()))
	}

	if strings.Trim(j.Name, " ") == "" {
		j.Name = "UNK"
	}

	return
}
