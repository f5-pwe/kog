package actions

import (
	"encoding/json"
)

type ActionStatus int

var actionstatusStrings = [...]string{
	"invalid",
	"success",
	"fail",
	"failure",
	"abort",
	"timeout",
}

const (
	ACTION_INVALID ActionStatus = iota
	ACTION_SUCCESS
	ACTION_FAIL
	ACTION_FAILURE
	ACTION_ABORT
	ACTION_TIMEOUT
)

func (as ActionStatus) String() string {
	return actionstatusStrings[as]
}

func (as ActionStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(as.String())
}

func (as *ActionStatus) UnmarshalJSON(b []byte) (err error) {
	var val string
	err = json.Unmarshal(b, &val)
	if err != nil {
		*as = ACTION_INVALID
		return
	}

	for idx, str := range actionstatusStrings {
		if str == val {
			*as = ActionStatus(idx)
			return
		}
	}

	*as = ACTION_INVALID
	return
}
