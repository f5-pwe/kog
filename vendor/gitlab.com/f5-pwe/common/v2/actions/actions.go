/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package actions

import (
	"encoding/json"
	"fmt"

	"gitlab.com/f5-pwe/common/v2/variables"
)

type Actions map[string]*Action

type tmpA Actions

func (a *Actions) UnmarshalJSON(b []byte) (err error) {
	var newA tmpA
	if err = json.Unmarshal(b, &newA); err == nil {
		for name, action := range newA {
			action.Name = name
			newA[name] = action
		}
		*a = Actions(newA)
	}
	return
}

func (a Actions) Action(name string) (*Action, error) {
	if action, ok := a[name]; ok {
		return action, nil
	}

	return &Action{}, fmt.Errorf("Action %s does not exist", name)
}

type Action struct {
	Name         string         `json:"-" yaml:"-"`
	Image        string         `json:"image" yaml:"image"`
	Tag          string         `json:"version" yaml:"version"`
	Command      string         `json:"command,omitempty" yaml:"command,omitempty"`
	Args         variables.Args `json:"args,omitempty" yaml:"args,omitempty"`
	Env          variables.Env  `json:"env,omitempty" yaml:"env,omitempty"`
	Resources    *PodResources  `json:"resources,omitempty" yaml:"resources,omitempty"`
	Requirements *Requirements  `json:"requirements,omitempty" yaml:"requirements,omitempty"`
	Output       *Output        `json:"output,omitempty" yaml:"output,omitempty"`
}
