package pwe

import (
	"encoding/json"
)

type JobStatus int

const (
	JOB_QUEUED JobStatus = iota
	JOB_RUNNING
	JOB_PENDING_RESOURCES
	JOB_FAILURE
	JOB_SUCCESS
	JOB_CANCELED
	JOB_UNDER_INVESTIGATION
)

var returnCodes = map[JobStatus]int{
	JOB_SUCCESS:             0,
	JOB_FAILURE:             1,
	JOB_RUNNING:             2,
	JOB_CANCELED:            3,
	JOB_QUEUED:              99,
	JOB_PENDING_RESOURCES:   100,
	JOB_UNDER_INVESTIGATION: 200,
}

var jobStatusStrings = map[JobStatus]string{
	JOB_SUCCESS:             "success",
	JOB_FAILURE:             "failure",
	JOB_RUNNING:             "running",
	JOB_CANCELED:            "canceled",
	JOB_QUEUED:              "queued",
	JOB_PENDING_RESOURCES:   "pending resources",
	JOB_UNDER_INVESTIGATION: "under investigation",
}

var jobStatusStrings2Status = map[string]JobStatus{
	"success":             JOB_SUCCESS,
	"failure":             JOB_FAILURE,
	"running":             JOB_RUNNING,
	"canceled":            JOB_CANCELED,
	"queued":              JOB_QUEUED,
	"pending resources":   JOB_PENDING_RESOURCES,
	"under investigation": JOB_UNDER_INVESTIGATION,
}

func (js JobStatus) String() string {
	return jobStatusStrings[js]
}

func JobStatusFromString(val string) JobStatus {
	if status, ok := jobStatusStrings2Status[val]; ok {
		return status
	}

	return JOB_FAILURE
}

func (js JobStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(js.String())
}

func (js JobStatus) ReturnCode() int {
	return returnCodes[js]
}

func (js *JobStatus) UnmarshalJSON(b []byte) (err error) {
	var val string
	err = json.Unmarshal(b, &val)
	if err != nil {
		*js = JOB_FAILURE
		return
	}

	*js = JobStatusFromString(val)
	return
}
