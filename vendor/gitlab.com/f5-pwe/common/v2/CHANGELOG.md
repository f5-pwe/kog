

<!--- next entry here -->

## 2.0.0
2024-07-13

### Breaking changes

#### BREAKING CHANGE: remove extra fields (d2ad3ca9208301a9e0f65ef970892e34f0d63c1c)

remove extra fields

* executor - this field was never used, was in theory going to be used
  for a dispatch system, but every customers dispatch system is unique
  enough that a field to work for all did not make sense.
* current_step changed from struct to string. There was no reason to
  hold a whole copy of the step structure when you just need the name to
  pull the step details out of the current workflow
* Added: action.Requirements that can contain the required env vars and
  context paths
* Added: action.Output which will contain the context structure this
  action is expected to output
* Added step.Timeout whch is the duration in string format (eg. 30m)
  instead of just seconds which step.TimeoutSeconds is.

### Features

- reuse duration in workflow.Step and actions.ActionResult (89b93e34d031e36ac5bde8d1578209f77cba02ce)

### Fixes

- get tests passing again (9dcf7e5c758ea9f9d32250eeb473dccefc229098)

## 1.0.0
2019-03-28

### Fixes

- add semver (0ddf0af109d59018a799beb2ef39154e43349065)

