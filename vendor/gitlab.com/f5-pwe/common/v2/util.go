/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pwe

import (
	"strconv"
	"time"
)

// Generate a new uuid string
type RequestID string

func NewRequestID() (id RequestID) {
	return RequestID(strconv.FormatInt(time.Now().UnixNano(), 36))
}

func (r RequestID) UnixNano() (nano int64, err error) {
	return strconv.ParseInt(r.String(), 36, 64)
}

func (r RequestID) String() string {
	return string(r)
}

func (r RequestID) GoString() string {
	return string(r)
}

func String2Pointer(s string) *string {
	return &s
}
