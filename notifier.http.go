/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package kog

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"path"
	"time"

	pwe "gitlab.com/f5-pwe/common/v2"
	"go.uber.org/zap"
)

type httpNotifyHook struct {
	BaseURL string
	Client  *http.Client
	logger  *zap.Logger
}

func NewHTTPNotifyHook(ctx context.Context, baseURL string) NotifyHook {
	client := &http.Client{Timeout: time.Second * 60}
	hook := &httpNotifyHook{
		BaseURL: baseURL,
		Client:  client,
	}
	_, hook.logger = ContextLogger(ctx, "notify", zap.String("component", "http-notifier"))
	return hook
}

func (h *httpNotifyHook) FireNotification(job *pwe.Job) error {
	body, err := json.Marshal(job)
	if err != nil {
		h.logger.Error("failed to marshal job", zap.Error(err))
		return err
	}

	u, err := h.buildURL("job", job.ID.String())
	if err != nil {
		h.logger.Error("failed to build url", zap.Error(err))
		return err
	}
	h.logger.Debug("HTTP hook request", zap.String("url", u), zap.String("body", string(body)))

	resp, err := h.Client.Post(u, "application/json", bytes.NewBuffer(body))
	if err != nil {
		h.logger.Error("failed to send HTTP request", zap.Error(err))
		return err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		h.logger.Error("failed to read response body", zap.Error(err))
		return err
	}
	h.logger.Debug("HTTP hook response", zap.String("url", u), zap.String("status", resp.Status), zap.String("body", string(respBody)))

	return nil
}

func (h *httpNotifyHook) buildURL(parts ...string) (string, error) {
	u, err := url.Parse(h.BaseURL)
	if err != nil {
		return "", err
	}
	u.Path = path.Join(append([]string{u.Path}, parts...)...)
	return u.String(), err
}
