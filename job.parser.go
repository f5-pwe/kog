package kog

import (
	"bufio"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/ghodss/yaml"
	pwe "gitlab.com/f5-pwe/common/v2"
	"go.uber.org/zap"

	"gitlab.com/f5-pwe/common/v2/variables"
)

func FileOrURLReader(ctx context.Context, taskFilename string) (reader io.Reader, err error) {
	_, logger := ContextLogger(ctx, "FileOrURLReader", zap.String("file", taskFilename))
	var body []byte
	if taskFilename != "" && !strings.HasPrefix(taskFilename, "http") {
		logger.Info("Reading local file")
		body, err = os.ReadFile(taskFilename)
		reader = strings.NewReader(string(body))
	} else if taskFilename != "" && strings.HasPrefix(taskFilename, "http") {
		logger.Info("Reading remote file")
		client := &http.Client{Timeout: time.Second * 60}
		var resp *http.Response
		resp, err = client.Get(taskFilename)
		if err != nil {
			return
		}
		defer resp.Body.Close()
		if err = checkResponse(resp); err != nil {
			return
		}
		body, err = io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		reader = strings.NewReader(string(body))
	} else {
		// stdin
		reader = bufio.NewReader(os.Stdin)
	}
	return
}

func ParseContext(data []byte) (ctx variables.Context, err error) {
	if err = json.Unmarshal(data, &ctx); err != nil {
		if err = yaml.Unmarshal(data, &ctx); err != nil {
			return nil, err
		}
	}

	return ctx, nil
}

func ParseJob(data []byte) (job *pwe.Job, err error) {
	if err = json.Unmarshal(data, &job); err != nil {
		if err = yaml.Unmarshal(data, &job); err != nil {
			return nil, err
		}
	}
	// Get the task id
	if job.ID == "" {
		job.ID = pwe.NewRequestID()
	}
	if job.ExecutionID == "" {
		job.ExecutionID = pwe.NewRequestID()
	}

	return job, nil
}
