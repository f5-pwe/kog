package kog

import "io"

type dockerJob struct {
	name, containerId string
	output            io.ReadCloser
	input             io.WriteCloser
}

func (dj *dockerJob) ID() string {
	return dj.containerId
}

func (dj *dockerJob) Input(input io.WriteCloser) io.WriteCloser {
	if input != nil {
		dj.input = input
	}

	return dj.input
}

func (dj *dockerJob) Output(output io.ReadCloser) io.ReadCloser {
	if output != nil {
		dj.output = output
	}

	return dj.output
}
