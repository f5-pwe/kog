package kog

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/digitalxero/slugify"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/pkg/stdcopy"
	pwe "gitlab.com/f5-pwe/common/v2"
	"gitlab.com/f5-pwe/common/v2/actions"
	"go.uber.org/zap"
)

const loggerName = "docker-executor"

type dockerExecutor struct {
	wc *wrappedClient

	sendSTDIn bool
	job       *dockerJob
	done      *sync.WaitGroup
}

func NewDockerExecutor(ctx context.Context) (e Executor, err error) {
	var wc *wrappedClient
	_, logger := ContextLogger(ctx, loggerName)
	if wc, err = newWrappedDockerClient(); err != nil {
		logger.Error("Error initializing Docker connection", zap.Error(err))
		return
	}
	e = &dockerExecutor{
		wc: wc,
	}

	return
}

func (de *dockerExecutor) GetJob() Job {
	return de.job
}

func (de *dockerExecutor) CreateJob(ctx context.Context, inputData, correlationId, jobName, stepName, runId string, action *actions.Action, labels map[string]string, volumes []string) (err error) {
	var (
		containerId string
	)
	ctx, logger := ContextLogger(ctx, loggerName)

	containerName := slugify.Slugify(fmt.Sprintf("%s-%s-%s-%s", correlationId, jobName, stepName, runId), 0)
	action.Env["KOG_CONTEXT"] = base64.StdEncoding.EncodeToString([]byte(inputData))
	containerId, de.sendSTDIn, err = de.wc.CreateContainer(
		ctx,
		containerName,
		action.Image,
		action.Tag,
		action.Command,
		action.Args,
		action.Env,
		volumes,
		labels,
	)
	if err != nil {
		logger.Error("Error creating container", zap.Error(err))
		return
	}

	de.job = &dockerJob{name: jobName, containerId: containerId}
	ctx, _ = ContextLogger(ctx, loggerName,
		zap.String("container_id", containerId),
		zap.String("container", jobName),
	)
	de.done = &sync.WaitGroup{}
	err = de.wc.StartContainer(ctx, de.job.ID())

	return
}

func (de *dockerExecutor) AttachIO(ctx context.Context) (err error) {
	var (
		input, output types.HijackedResponse
	)

	if de.sendSTDIn {
		if input, err = de.wc.AttachContainerStdin(ctx, de.job.ID()); err != nil {
			return
		}
		de.job.Input(input.Conn)
	}

	if output, err = de.wc.AttachContainerOutput(ctx, de.job.ID()); err != nil {
		return
	}
	de.job.Output(io.NopCloser(output.Reader))

	return
}

func (de *dockerExecutor) SendInput(inputData string) (err error) {
	if !de.sendSTDIn {
		return
	}

	var writer = de.job.input
	defer writer.Close()

	bufin := bufio.NewWriter(writer)
	if _, err = bufin.WriteString(inputData); err != nil {
		return
	}

	return bufin.Flush()
}

func (de *dockerExecutor) ProcessOutput(ctx context.Context, resultText *string) {
	outR, outW := io.Pipe()
	outTee := io.NopCloser(io.TeeReader(outR, os.Stdout))
	reader := bufio.NewReader(outTee)
	_, logger := ContextLogger(ctx, loggerName)

	// container output demuxing goroutine
	de.done.Add(1)
	go func() {
		defer outW.Close()
		defer de.done.Done()
		if _, err := stdcopy.StdCopy(outW, os.Stderr, de.job.output); err != nil {
			logger.Error("Error in container output streaming", zap.Error(err))
		}

		logger.Debug("Step standard streams copied")
	}()

	de.done.Add(1)
	// container output scanning goroutine
	go func() {
		defer outTee.Close()
		defer de.done.Done()
		for {
			line, err := reader.ReadString('\n')

			if strings.HasPrefix(line, "KOG:") {
				rt := strings.TrimLeft(line, "KOG:")
				rt = strings.TrimSpace(rt)
				logger.Debug("Got result", zap.String("raw_result", rt))
				*resultText = rt
				if _, err := io.ReadAll(outTee); err != nil {
					logger.Debug("Error in container output streaming", zap.Error(err))
				}
				break
			} else {
				logger.Debug(line)
			}

			if err == io.EOF {
				logger.Error("Leaving output scanning, got EOF from the docker logs before a result was generated!")
				break
			} else if err != nil {
				logger.Error("Error processing container output", zap.Error(err))
			}
		}
	}()
}

func (de *dockerExecutor) Wait(ctx context.Context, timeout time.Duration) (exitCode int, err error) {
	ctx, logger := ContextLogger(ctx, loggerName)

	if timedout := waitTimeout(ctx, de.done, timeout); timedout {
		logger.Error(fmt.Sprintf("step \"%v\" timed out after %v", de.job.name, timeout))

		if err = de.wc.KillContainer(ctx, de.job.ID()); err != nil && !strings.Contains(err.Error(), "No such container") {
			logger.Error("Error killing container", zap.Error(err))
			return
		}
		err = fmt.Errorf("step timeout")
		return
	}

	logger.Info("Waiting for container to exit...")
	if exitCode, err = de.wc.WaitForContainerExitWithTimeout(ctx, de.job.ID(), containerExitTimeout); err != nil {
		if strings.Contains(err.Error(), context.DeadlineExceeded.Error()) {
			err = fmt.Errorf("container exit timed out after %v: %w", containerExitTimeout, err)
		}
		logger.Error("Error getting container exit code", zap.Error(err), zap.Int("exitCode", exitCode))
		return
	}

	return 0, nil
}

func (de *dockerExecutor) Cleanup(ctx context.Context) (err error) {
	ctx, logger := ContextLogger(ctx, loggerName)

	if de.job != nil {
		if err = de.wc.RemoveContainer(ctx, de.job.ID()); err != nil && !strings.Contains(err.Error(), "No such container") {
			logger.Error("Failed to remove container after successful execution", zap.Error(err))
		} else {
			logger.Debug("Container Removed")
		}
	}

	de.job = nil
	de.done = nil
	return
}

func (de *dockerExecutor) Prepare(ctx context.Context, job *pwe.Job) (err error) {
	ctx, logger := ContextLogger(ctx, loggerName)
	logger.Info("Pulling docker images")
	wg := &sync.WaitGroup{}
	for _, v := range job.Actions {
		wg.Add(1)
		go func(v *actions.Action) {
			defer wg.Done()
			logger.Debug("Pulling image",
				zap.String("image", v.Image),
				zap.String("tag", v.Tag),
				zap.String("action", v.Name),
			)
			tag := "latest"
			if v.Tag != "" {
				tag = v.Tag
			}
			if perr := de.wc.PullImage(ctx, v.Image, tag); perr != nil {
				logger.Error("Error pulling image", zap.Error(perr))
				err = perr
			}
		}(v)
	}
	wg.Wait()
	return
}
