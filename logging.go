package kog

import (
	"context"

	"go.uber.org/zap"
)

type ctxKey int

const LoggerContextKey ctxKey = iota

func ContextLogger(ctx context.Context, name string, fields ...zap.Field) (context.Context, *zap.Logger) {
	if logger, ok := ctx.Value(LoggerContextKey).(*zap.Logger); ok {
		logger = logger.With(fields...)

		return context.WithValue(ctx, LoggerContextKey, logger), logger
	}
	logger := zap.L().With(fields...).Named(name)

	return context.WithValue(ctx, LoggerContextKey, logger), logger
}
