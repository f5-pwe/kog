package kog

import (
	"encoding/json"
	"fmt"

	"gitlab.com/f5-pwe/common/v2/actions"
	"gitlab.com/f5-pwe/common/v2/variables"
)

func DecodeResults(result []byte) (actions.ActionResult, error) {
	resultStr := string(result)
	if resultStr == "" {
		err := fmt.Errorf("result is empty")
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}
	if resultStr == "{}" {
		err := fmt.Errorf("JSON result is empty")
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}
	var stepRunResult actions.ActionResult
	if err := json.Unmarshal(result, &stepRunResult); err != nil {
		err = fmt.Errorf("parsing JSON: %w", err)
		return NewStepResult(actions.ACTION_ABORT, err.Error()), err
	}

	return stepRunResult, nil
}

func DecodeStepResults(ctx variables.Context) (results []actions.ActionResult, err error) {
	var (
		b []byte
	)
	r, ok := ctx["step_run_results"]
	if !ok {
		return []actions.ActionResult{}, nil
	}

	// not the best solution, but works reliably
	// we need to specifically unmarshal step_run_results
	b, err = json.Marshal(r)
	if err != nil {
		return nil, fmt.Errorf("serializing step run results: %v", err)
	}
	if err = json.Unmarshal(b, &results); err != nil {
		return nil, fmt.Errorf("parsing step run results: %v", err)
	}

	return results, nil
}
