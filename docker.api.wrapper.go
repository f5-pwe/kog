/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kog

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/image"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/drone/envsubst"
	"go.uber.org/zap"
)

type wrappedClient struct {
	cli *client.Client
}

func newWrappedDockerClient() (wc *wrappedClient, err error) {
	wc = &wrappedClient{}
	wc.cli, err = client.NewClientWithOpts(client.FromEnv)

	return
}

func (wc *wrappedClient) PullImage(ctx context.Context, imageName string, tag string) error {
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	ctx, logger := ContextLogger(ctx, "docker-client",
		zap.String("fullImageName", imageName),
		zap.String("tag", imageTag),
	)
	opts := image.PullOptions{}
	fullImageName := fmt.Sprintf("%s:%s", imageName, imageTag)
	output, err := wc.cli.ImagePull(ctx, fullImageName, opts)
	if err != nil {
		return err
	}
	outdec := json.NewDecoder(output)
	type statusLine struct {
		Id       string `json:"id"`
		Status   string `json:"status"`
		Progress string `json:"progress"`
		Error    string `json:"error"`
	}
	for {
		line := statusLine{}
		if err = outdec.Decode(&line); err == io.EOF {
			break
		} else if err != nil {
			logger.Error("failed to decode line", zap.Error(err))
		} else if line.Error != "" {
			return fmt.Errorf("%v", line.Error)
		}
	}
	return nil
}

func (wc *wrappedClient) CreateContainer(ctx context.Context, containerName string, imageName string, tag string,
	command string, args []string, env map[string]string, volumes []string, labels map[string]string) (containerID string, useSTDIn bool, err error) {
	var resp container.CreateResponse
	useSTDIn = true
	imageTag := tag
	if tag == "" {
		imageTag = "latest"
	}
	ctx, logger := ContextLogger(ctx, "docker-client",
		zap.String("fullImageName", imageName),
		zap.String("tag", imageTag),
	)
	fullImageName := fmt.Sprintf("%s:%s", imageName, imageTag)
	imageInfo, body, err := wc.cli.ImageInspectWithRaw(ctx, fullImageName)
	if err != nil && client.IsErrNotFound(err) {
		if err = wc.PullImage(ctx, imageName, tag); err != nil {
			logger.Error("Failed to pull missing image", zap.Error(err))
			return
		}
		imageInfo, _, err = wc.cli.ImageInspectWithRaw(ctx, fullImageName)
	}

	if err != nil {
		if body != nil {
			logger.Error("fullImageName details", zap.String("body", string(body)))
		}

		logger.Error("Error fetching fullImageName details", zap.Error(err))
		return
	}

	containerEnv := []string{}
	for k, v := range env {
		containerEnv = append(containerEnv, fmt.Sprintf("%s=%s", k, v))
		os.Setenv(k, v)
	}

	if imageInfo.Config != nil {
		for _, imgEnv := range imageInfo.Config.Env {
			kv := strings.Split(imgEnv, "=")
			os.Setenv(kv[0], kv[1])
			if kv[0] == "KOG_ENV_CONTEXT" && kv[1] == "true" {
				useSTDIn = false
			}
		}
	}
	for idx, eEnv := range containerEnv {
		if eEnv, err = envsubst.EvalEnv(eEnv); err == nil {
			containerEnv[idx] = eEnv
		}
	}
	containerLabels := make(map[string]string)
	for k, v := range labels {
		containerLabels[k] = v
	}

	stopTimeout, _ := strconv.Atoi(containerExitTimeout.String())
	if stopTimeout < 10 {
		stopTimeout = 10
	}

	containerConf := container.Config{
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Tty:          false,
		OpenStdin:    true,
		StdinOnce:    true,
		Env:          containerEnv,
		Image:        fullImageName,
		StopSignal:   "SIGTERM",
		StopTimeout:  &stopTimeout,
		Labels:       containerLabels,
	}
	logger.Debug("Container Config", zap.Any("config", containerConf))
	if command != "" {
		cmd := []string{command}
		cmd = append(cmd, args...)
		containerConf.Cmd = cmd
	} else if len(args) > 0 {
		cmd := imageInfo.Config.Cmd
		cmd = append(cmd, args...)
		containerConf.Cmd = cmd
	}
	logger.Debug("Container command", zap.Strings("cmd", containerConf.Cmd))

	hostConf := container.HostConfig{}
	if volumes != nil {
		for idx, vol := range volumes {
			if vol, err := envsubst.EvalEnv(vol); err == nil {
				volumes[idx] = vol
			}
		}
		hostConf.Binds = volumes
	}

	netConf := network.NetworkingConfig{}
	if resp, err = wc.cli.ContainerCreate(ctx, &containerConf, &hostConf, &netConf, nil, containerName); err != nil {
		return
	}
	containerID = resp.ID

	return
}

func (wc *wrappedClient) AttachContainerStdin(ctx context.Context, containerId string) (types.HijackedResponse, error) {
	attachOpts := container.AttachOptions{
		Stream: true,
		Stdin:  true,
		Stdout: false,
		Stderr: false,
	}
	resp, err := wc.cli.ContainerAttach(
		ctx,
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func (wc *wrappedClient) AttachContainerOutput(ctx context.Context, containerId string) (types.HijackedResponse, error) {
	attachOpts := container.AttachOptions{
		Stream: true,
		Stdin:  false,
		Stdout: true,
		Stderr: true,
	}
	resp, err := wc.cli.ContainerAttach(
		ctx,
		containerId,
		attachOpts,
	)
	if err != nil {
		return types.HijackedResponse{}, err
	}
	return resp, nil
}

func (wc *wrappedClient) StartContainer(ctx context.Context, containerId string) error {
	if err := wc.cli.ContainerStart(ctx, containerId, container.StartOptions{}); err != nil {
		return err
	}
	return nil
}

func (wc *wrappedClient) WaitForContainerExitWithTimeout(ctx context.Context, containerId string, timeout time.Duration) (statusCode int, err error) {
	resp, err := wc.cli.ContainerInspect(ctx, containerId)
	if err != nil {
		return -1, err
	}

	state := resp.State
	if state != nil && state.Status == "exited" {
		return state.ExitCode, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	result, werr := wc.cli.ContainerWait(ctx, containerId, container.WaitConditionNotRunning)
	statusCode = 99
	select {
	case <-ctx.Done():
		err = ctx.Err()
	case out := <-result:
		statusCode = int(out.StatusCode)
		if out.Error != nil {
			err = fmt.Errorf(out.Error.Message)
		}
	case err = <-werr:
		break
	}

	return
}

func (wc *wrappedClient) GetContainerStdout(ctx context.Context, containerId string) (io.ReadCloser, error) {
	stdoutOpts := container.LogsOptions{
		ShowStdout: true,
		ShowStderr: false,
		Timestamps: false,
	}
	return wc.cli.ContainerLogs(ctx, containerId, stdoutOpts)
}

func (wc *wrappedClient) GetContainerStderr(ctx context.Context, containerId string) (io.ReadCloser, error) {
	stderrOpts := container.LogsOptions{
		ShowStdout: false,
		ShowStderr: true,
	}
	return wc.cli.ContainerLogs(ctx, containerId, stderrOpts)
}

func (wc *wrappedClient) RemoveContainer(ctx context.Context, containerId string) error {
	opts := container.RemoveOptions{
		Force:         false,
		RemoveVolumes: false,
		RemoveLinks:   false,
	}
	if err := wc.cli.ContainerRemove(ctx, containerId, opts); err != nil && client.IsErrNotFound(err) {
		return err
	}
	return nil
}

func (wc *wrappedClient) KillContainer(ctx context.Context, containerId string) error {
	if err := wc.cli.ContainerKill(ctx, containerId, "SIGKILL"); err != nil && client.IsErrNotFound(err) {
		return err
	}
	return nil
}
