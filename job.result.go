package kog

import (
	pwe "gitlab.com/f5-pwe/common/v2"
)

type JobResult struct {
	ID            string        `json:"correlation_id" yaml:"correlation_id"`
	Name          string        `json:"task_name" yaml:"task_name"`
	Status        pwe.JobStatus `json:"status" yaml:"status"`
	Message       string        `json:"message" yaml:"message"`
	NotifyStatus  string        `json:"notify_status" yaml:"notify_status"`
	NotifyMessage string        `json:"notify_message" yaml:"notify_message"`
}
