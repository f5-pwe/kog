package kog

import (
	"context"
	"io"
	"time"

	pwe "gitlab.com/f5-pwe/common/v2"
	"gitlab.com/f5-pwe/common/v2/actions"
)

const containerExitTimeout = time.Second * 30
const containerStartTimeout = time.Minute * 15

type Job interface {
	ID() string
	Input(input io.WriteCloser) io.WriteCloser
	Output(output io.ReadCloser) io.ReadCloser
}

type Executor interface {
	Prepare(ctx context.Context, job *pwe.Job) error
	CreateJob(ctx context.Context, inputData, correlationId, jobName, stepName, runId string, action *actions.Action, labels map[string]string, volumes []string) error
	GetJob() Job
	AttachIO(ctx context.Context) error
	SendInput(inputData string) error
	ProcessOutput(ctx context.Context, resultText *string)
	Wait(ctx context.Context, timeout time.Duration) (int, error)
	Cleanup(ctx context.Context) error
}
