package kog

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"strings"
	"sync"
	"time"

	"github.com/digitalxero/slugify"
	pwe "gitlab.com/f5-pwe/common/v2"
	"gitlab.com/f5-pwe/common/v2/actions"
	"go.uber.org/zap"
	batchapiv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	clientv1 "k8s.io/client-go/kubernetes/typed/core/v1"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	pointer2 "k8s.io/utils/pointer"
)

type k8sExecutor struct {
	config     *rest.Config
	client     rest.Interface
	namespace  string
	clientset  *kubernetes.Clientset
	podsClient clientv1.PodInterface

	job  *k8sJob
	done *sync.WaitGroup
}

func Newk8sExecutor(ctx context.Context) (e Executor, err error) {
	ke := &k8sExecutor{}
	configRules := clientcmd.NewDefaultClientConfigLoadingRules()
	overrides := &clientcmd.ConfigOverrides{}
	deferred := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(configRules, overrides)

	if ke.config, err = deferred.ClientConfig(); err != nil {
		return
	}

	if ke.namespace, _, err = deferred.Namespace(); err != nil {
		return
	}

	if ke.clientset, err = kubernetes.NewForConfig(ke.config); err != nil {
		return
	}

	ke.client = ke.clientset.CoreV1().RESTClient()
	ke.podsClient = ke.clientset.CoreV1().Pods(ke.namespace)

	e = ke

	return
}

func (k *k8sExecutor) GetJob() Job {
	return k.job
}

func (k *k8sExecutor) CreateJob(ctx context.Context, inputData, correlationId, jobName, stepName, runId string, action *actions.Action, labels map[string]string, volumes []string) (err error) {
	var (
		watcher watch.Interface
	)
	ctx, logger := ContextLogger(ctx, "docker-executor",
		zap.String("job", jobName),
		zap.String("step", stepName),
		zap.String("run", runId),
		zap.String("correlation_id", correlationId),
		zap.Any("labels", labels),
	)

	if len(volumes) > 0 {
		logger.Error("k8s executor type does not currently support volume mounts", zap.Strings("volumes", volumes))
	}

	k.job = &k8sJob{
		client:    k.clientset.BatchV1().Jobs(k.namespace),
		logsSince: metav1.Now(),
	}

	args := make([]string, 0)
	if action.Command != "" {
		args = append(args, action.Command)
	}
	for _, arg := range action.Args {
		args = append(args, arg)
	}
	if len(args) == 0 {
		args = nil
	}

	slugify.TO_DASH = fmt.Sprintf("%s_", slugify.TO_DASH)
	slugify.OK = "-"

	k.job.k8sJobName = slugify.Slugify(fmt.Sprintf("%s-%s-%s-%s", correlationId, runId, stepName, jobName), 50)
	if _, err = k.clientset.CoreV1().Secrets(k.namespace).Create(ctx,
		&apiv1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:   k.job.k8sJobName,
				Labels: labels,
			},
			StringData: action.Env,
		},
		metav1.CreateOptions{},
	); err != nil {
		logger.Error("failed to create env secrets", zap.Error(err))
		return
	}

	k.job.k8sType = &batchapiv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:   k.job.k8sJobName,
			Labels: labels,
		},
		Spec: batchapiv1.JobSpec{
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:            action.Name,
							Image:           fmt.Sprintf("%s:%s", action.Image, action.Tag),
							ImagePullPolicy: apiv1.PullAlways,
							Args:            args,
							Stdin:           false,
							TTY:             false,
							Env: []apiv1.EnvVar{
								{
									Name:  "KOG_CONTEXT",
									Value: base64.StdEncoding.EncodeToString([]byte(inputData)),
								},
							},
							Resources: getResourceReqs(action.Resources),
							EnvFrom: []apiv1.EnvFromSource{
								{
									SecretRef: &apiv1.SecretEnvSource{
										LocalObjectReference: apiv1.LocalObjectReference{
											Name: k.job.k8sJobName,
										},
									},
								},
							},
						},
					},
					RestartPolicy: apiv1.RestartPolicyNever,
				},
			},
			BackoffLimit: pointer2.Int32Ptr(1),
		},
	}

	if err = k.job.Create(ctx); err != nil {
		return
	}

	if watcher, err = k.podsClient.Watch(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("job-name=%s", k.job.ID()),
	}); err != nil {
		return
	}
	defer watcher.Stop()
	events := watcher.ResultChan()
	done := false
	for {
		if done {
			break
		}
		select {
		case evt, more := <-events:
			if !more {
				logger.Error("failed to get event in creating pod")
				done = true
				break
			}
			pod, ok := evt.Object.(*apiv1.Pod)
			if !ok {
				break
			}
			k.job.pod = pod

			for _, cond := range pod.Status.Conditions {
				if (cond.Type == apiv1.PodReady &&
					cond.Status == apiv1.ConditionTrue) ||
					pod.Status.Phase == apiv1.PodFailed {
					done = true
					break
				}
			}
		case <-time.After(containerStartTimeout):
			logger.Error(fmt.Sprintf("container failed to start after %d minutes", containerStartTimeout/time.Minute))
			done = true
			break
		case <-ctx.Done():
			logger.Error("app context exited")
			done = true
			break
		}
	}

	ContextLogger(ctx, "docker-executor", zap.String("container_id", k.job.k8sType.Name), zap.String("container", jobName))
	k.done = new(sync.WaitGroup)

	return
}

func (k *k8sExecutor) AttachIO(ctx context.Context) (err error) {
	req := k.podsClient.GetLogs(k.job.pod.Name, &apiv1.PodLogOptions{
		Follow:    true,
		SinceTime: &k.job.logsSince,
	})

	k.job.output, err = req.Stream(ctx)

	return
}

func (k *k8sExecutor) SendInput(inputData string) (err error) {
	return
}

func (k *k8sExecutor) ProcessOutput(ctx context.Context, resultText *string) {
	ctx, logger := ContextLogger(ctx, "docker-executor", zap.Stringp("output", resultText))
	k.done.Add(1)
	go func() {
		// outTee := ioutil.NopCloser(io.TeeReader(k.job.output, os.Stdout))
		bufReader := bufio.NewReader(k.job.output)
		defer k.job.output.Close()
		// defer outTee.Close()
		defer k.done.Done()
		reattachCount := 0
		for {
			line, err := bufReader.ReadString('\n')

			if strings.HasPrefix(line, "KOG:") {
				rt := strings.TrimLeft(line, "KOG:")
				rt = strings.TrimSpace(rt)
				logger.Debug("Got result", zap.String("raw_result", rt))
				*resultText = rt
				break
			}

			if err == io.EOF {
				if strings.Contains(line, "failed to create fsnotify watcher") {
					if reattachCount%20 == 0 {
						fmt.Println("\n============ start kog notice ============")
						fmt.Println("error reading pod logs -", line, "- https://github.com/kubernetes/kubernetes/issues/64315")
						fmt.Println("Check the open file handles on all of your k8s nodes, including the masters")
						fmt.Println("Reattaching the log stream, you may see some duplicate logs")
						fmt.Println("============= end kog notice =============\n ")
					}

					reattachCount++
					k.job.output.Close()
					if e := k.AttachIO(ctx); e != nil {
						time.Sleep(250 * time.Millisecond)
						continue
					}
					bufReader = bufio.NewReader(k.job.output)
					time.Sleep(250 * time.Millisecond)
					continue
				}

				fmt.Print(line)
				logger.Error("Got EOF from the k8s pod logs before a result was generated!")
				break
			} else if err != nil {
				fmt.Print(line)
				logger.Error("Error processing container output", zap.Error(err))
				break
			}

			k.job.logsSince = metav1.Now()
			fmt.Print(line)
			logger.Debug(line)
			if reattachCount > 0 {
				reattachCount = -2
			}
		}
	}()
}

func (k *k8sExecutor) Wait(ctx context.Context, timeout time.Duration) (exitCode int, err error) {
	ctx, logger := ContextLogger(ctx, "docker-executor", zap.Duration("timeout", timeout))
	k.done.Add(1)
	done := false
	go func() {
		defer k.done.Done()
		var watcher watch.Interface
		if watcher, err = k.job.Watch(ctx); err != nil {
			return
		}
		defer watcher.Stop()
		events := watcher.ResultChan()
		for {
			if done {
				break
			}
			evt, more := <-events
			if !more {
				logger.Error("failed to get event in waiting jobs")
				done = true
				break
			}
			job, ok := evt.Object.(*batchapiv1.Job)
			if !ok {
				break
			}
			k.job.k8sType = job

			for _, cond := range job.Status.Conditions {
				if (cond.Type == batchapiv1.JobComplete || cond.Type == batchapiv1.JobFailed) &&
					cond.Status == apiv1.ConditionTrue {
					done = true
					break
				}
			}
		}
	}()

	if timedout := waitTimeout(ctx, k.done, timeout); timedout {
		done = true
		if err = k.job.Delete(ctx); err != nil {
			logger.Error("Error deleting job", zap.Error(err))
		}
		logger.Error(fmt.Sprintf("step \"%v\" timed out after %v", k.job.ID(), timeout))
		return
	}

	if k.job.k8sType.Status.Failed > 0 {
		exitCode = int(k.job.k8sType.Status.Failed)
	}

	return
}

func (k *k8sExecutor) Cleanup(ctx context.Context) (err error) {
	ctx, logger := ContextLogger(ctx, "docker-executor")

	if k.job != nil {
		if err = k.job.Delete(ctx); err != nil && err.Error() != "resource name may not be empty" {
			logger.Error("Error deleting job", zap.Error(err))
		}
		if err = k.clientset.CoreV1().Secrets(k.namespace).Delete(ctx, k.job.k8sJobName, metav1.DeleteOptions{}); err != nil {
			logger.Error("Error deleting job secrets", zap.Error(err))
		}
	}

	k.job = nil
	k.done = nil
	return
}

func (k *k8sExecutor) Prepare(ctx context.Context, job *pwe.Job) (err error) {
	_, logger := ContextLogger(ctx, "docker-executor")

	var info *version.Info
	if info, err = k.clientset.DiscoveryClient.ServerVersion(); err != nil {
		return
	}
	logger.Info(fmt.Sprintf("k8s server version: %s", info.String()))

	return
}

func getResourceReqs(resources *actions.PodResources) apiv1.ResourceRequirements {
	cpuRequest, _ := resource.ParseQuantity("0.1")
	memoryRequest, _ := resource.ParseQuantity("256Mi")
	cpuLimit, _ := resource.ParseQuantity("1")
	memoryLimit, _ := resource.ParseQuantity("1.5Gi")
	if resources != nil {
		cpuRequest, _ = resource.ParseQuantity(resources.Requests.CPU)
		memoryRequest, _ = resource.ParseQuantity(resources.Requests.Memory)
		cpuLimit, _ = resource.ParseQuantity(resources.Limits.CPU)
		memoryLimit, _ = resource.ParseQuantity(resources.Limits.Memory)
	}
	return apiv1.ResourceRequirements{
		Requests: apiv1.ResourceList{
			apiv1.ResourceCPU:    cpuRequest,
			apiv1.ResourceMemory: memoryRequest,
		},
		Limits: apiv1.ResourceList{
			apiv1.ResourceCPU:    cpuLimit,
			apiv1.ResourceMemory: memoryLimit,
		},
	}
}
