package kog

import (
	"context"
	"io"

	batchapiv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	batchv1 "k8s.io/client-go/kubernetes/typed/batch/v1"
)

type k8sJob struct {
	k8sJobName string
	k8sType    *batchapiv1.Job
	pod        *apiv1.Pod
	client     batchv1.JobInterface
	output     io.ReadCloser
	input      io.WriteCloser
	logsSince  metav1.Time
}

func (kj *k8sJob) ID() string {
	return kj.k8sType.Name
}

func (kj *k8sJob) Create(ctx context.Context) (err error) {
	kj.k8sType, err = kj.client.Create(ctx, kj.k8sType, metav1.CreateOptions{})

	return
}

func (kj *k8sJob) Delete(ctx context.Context) (err error) {
	deletePolicy := metav1.DeletePropagationForeground
	return kj.client.Delete(ctx, kj.ID(), metav1.DeleteOptions{PropagationPolicy: &deletePolicy})
}

func (kj *k8sJob) UpdateStatus(ctx context.Context) (err error) {
	kj.k8sType, err = kj.client.UpdateStatus(ctx, kj.k8sType, metav1.UpdateOptions{})

	return
}

func (kj *k8sJob) Update(ctx context.Context) (err error) {
	kj.k8sType, err = kj.client.Update(ctx, kj.k8sType, metav1.UpdateOptions{})

	return
}

func (kj *k8sJob) Watch(ctx context.Context) (watcher watch.Interface, err error) {
	watcher, err = kj.client.Watch(ctx, metav1.SingleObject(kj.k8sType.ObjectMeta))

	return
}

func (kj *k8sJob) Input(input io.WriteCloser) io.WriteCloser {
	if input != nil {
		kj.input = input
	}

	return kj.input
}

func (kj *k8sJob) Output(output io.ReadCloser) io.ReadCloser {
	if output != nil {
		kj.output = output
	}

	return kj.output
}
