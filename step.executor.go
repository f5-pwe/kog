package kog

import (
	"context"
	"encoding/json"
	"fmt"
	_ "io"
	"time"

	"github.com/digitalxero/slugify"
	"github.com/mdaverde/jsonpath"
	pwe "gitlab.com/f5-pwe/common/v2"
	"go.uber.org/zap"

	"gitlab.com/f5-pwe/common/v2/actions"
	"gitlab.com/f5-pwe/common/v2/variables"
	"gitlab.com/f5-pwe/common/v2/workflow"
)

const (
	defaultTimeout = time.Hour * 24
)

func ExecuteStep(ctx context.Context, executor Executor, step *workflow.Step, job *pwe.Job, jobCTX variables.Context,
	notifierHooks *NotifyHooks, extraEnv map[string]string, volumes []string) actions.ActionResult {
	ctx, logger := ContextLogger(ctx, "step-executor", zap.String("step", step.Name))
	var (
		err           error
		action        *actions.Action
		result        actions.ActionResult
		inputJsonData []byte
		// input io.WriteCloser
		// output io.ReadCloser
		exitCode int
	)

	defer func() {
		_ = executor.Cleanup(ctx)
		if r := recover(); r != nil {
			logger.Panic("Panic in ExecuteStep", zap.Any("err", r))
		}
	}()

	actionLogger := logger.With(zap.String("action", step.ActionName))
	if action, err = job.Actions.Action(step.ActionName); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Failed to get action \"%v\" for step \"%v\": %v", step.ActionName, step.Name, err.Error()),
		)
	}

	if extraEnv != nil {
		if action.Env == nil {
			action.Env = make(variables.Env)
		}

		for key, value := range extraEnv {
			action.Env[key] = value
		}
	}

	if action.Requirements != nil {
		for _, key := range action.Requirements.Env {
			if _, ok := action.Env[key]; !ok {
				return NewStepResult(
					actions.ACTION_ABORT,
					fmt.Sprintf(`Failed to execute action "%v" for step "%v": missing required environment variable %s`, step.ActionName, step.Name, key),
				)
			}
		}
		for _, path := range action.Requirements.ContextPaths {
			if _, err = jsonpath.Get(jobCTX, path); err != nil {
				return NewStepResult(
					actions.ACTION_ABORT,
					fmt.Sprintf(`Failed to execute action "%v" for step "%v": missing required context path %s`, step.ActionName, step.Name, path),
				)
			}
		}
	}

	// Create input context
	correlationId := job.ID.String()
	if _, ok := jobCTX["correlation_id"]; ok {
		correlationId = jobCTX["correlation_id"].(string)
	}
	runId := job.ExecutionID
	jobName := job.Name
	jobCTX["task_name"] = jobName
	jobCTX["run_id"] = runId
	jobCTX["correlation_id"] = correlationId
	jobCTX["step_name"] = step.Name
	jobCTX["action_name"] = action.Name
	containerTimeout := defaultTimeout
	if step.Timeout != nil {
		containerTimeout = step.Timeout.Duration
	} else if step.TimeoutSeconds > 0 {
		containerTimeout = time.Duration(step.TimeoutSeconds) * time.Second
	}

	actionLogger.Debug("Starting Container", zap.Duration("timeout", containerTimeout))

	if inputJsonData, err = json.Marshal(jobCTX); err != nil {
		return NewStepResult(
			actions.ACTION_ABORT,
			fmt.Sprintf("Invalid input context: %s", err.Error()),
		)
	}

	inputData := fmt.Sprintf("%s", inputJsonData)
	labels := make(map[string]string)
	labels["dev.pwe.job.runner"] = "kog"
	labels["dev.pwe.job.name"] = slugify.IDify(jobName, 60)
	labels["dev.pwe.job.step"] = slugify.IDify(step.Name, 60)
	labels["dev.pwe.job.action"] = slugify.IDify(action.Name, 60)
	labels["dev.pwe.job.run_id"] = slugify.IDify(runId.String(), 60)
	labels["dev.pwe.job.correlation_id"] = slugify.IDify(correlationId, 60)
	actionLogger.Debug("set labels", zap.Any("labels", labels))

	// fire notification hook just before starting the container step
	if err = notifierHooks.FireNotification(job); err != nil {
		actionLogger.Error("Notify hook failed", zap.Error(err))
	}

	if err = executor.CreateJob(ctx, inputData, correlationId, jobName, step.Name, runId.String(), action, labels, volumes); err != nil {
		actionLogger.Error("Error in create container", zap.Error(err))
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	if err = executor.AttachIO(ctx); err != nil {
		actionLogger.Error("Error in attaching container IO", zap.Error(err))
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	resultText := ""

	executor.ProcessOutput(ctx, &resultText)

	if err = executor.SendInput(inputData); err != nil {
		actionLogger.Error("Error writing container stdin", zap.Error(err))
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	actionLogger.Info("Step Running")

	if exitCode, err = executor.Wait(ctx, containerTimeout); err != nil {
		actionLogger.Error("Error waiting for step to complete", zap.Error(err))
		return NewStepResult(actions.ACTION_TIMEOUT, err.Error())
	}

	if result, err = DecodeResults([]byte(resultText)); err != nil {
		actionLogger.Error("Could not parse result", zap.Error(err), zap.String("result", resultText))
		return NewStepResult(actions.ACTION_ABORT, err.Error())
	}

	// Restore read-only keys
	(*result.Context)["task_name"] = jobName
	(*result.Context)["run_id"] = runId
	(*result.Context)["correlation_id"] = correlationId
	(*result.Context)["step_name"] = step.Name
	(*result.Context)["action_name"] = action.Name
	result.ReturnCode = exitCode
	result.Name = step.Name

	return result

}

func NewStepResult(result actions.ActionStatus, message string) actions.ActionResult {
	return actions.ActionResult{
		Result:       result,
		ReturnCode:   -1,
		Message:      message,
		NotifyStatus: "",
		Context:      &variables.Context{},
	}
}

func NextStep(step *workflow.Step, code actions.ActionStatus) string {
	switch code {
	case actions.ACTION_SUCCESS:
		return step.SuccessStep
	case actions.ACTION_FAIL:
		return step.FailStep
	case actions.ACTION_ABORT:
		return step.AbortStep
	}
	return ""
}
