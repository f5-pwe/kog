package kog

import (
	"context"
	"fmt"
	"time"

	pwe "gitlab.com/f5-pwe/common/v2"
	"gitlab.com/f5-pwe/common/v2/duration"
	"go.uber.org/zap"
	"k8s.io/utils/pointer"

	"gitlab.com/f5-pwe/common/v2/actions"
	"gitlab.com/f5-pwe/common/v2/workflow"
)

func ExecuteJob(ctx context.Context, executor Executor, job *pwe.Job, notifierHooks *NotifyHooks, extraEnv map[string]string, volumes []string) (retval JobResult) {
	ctx, logger := ContextLogger(ctx, "job-executor", zap.Any("extra_env", extraEnv))
	retval = JobResult{Name: job.Name, ID: job.ID.String()}
	var (
		err  error
		step *workflow.Step
	)

	defer func() {
		if r := recover(); r != nil {
			logger.Panic("Panic in ExecuteJob", zap.Any("err", r))
		}
	}()

	if err = executor.Prepare(ctx, job); err != nil {
		logger.Error("Error preparing environment to execute job", zap.Error(err))
		retval.Status = pwe.JOB_FAILURE
		retval.Message = "Error preparing environment to execute job"
		return
	}

	logger.Debug("Starting Workflow", zap.Any("job", job))
	job.Status = pwe.JOB_RUNNING

	next := "start"
	for {
		if step, err = job.Workflow().Step(next); err != nil {
			retval.Status = pwe.JOB_FAILURE
			retval.Message = fmt.Sprintf("unknown next step: \"%s\"", next)
			break
		}
		stepLogger := logger.With(zap.String("step", step.Name))
		job.CurrentStep = pointer.String(step.Name)

		startTime := time.Now()
		result := ExecuteStep(ctx, executor, step, job, *job.Context, notifierHooks, extraEnv, volumes)
		runTime := time.Since(startTime)
		logger.Debug("Step Finished",
			zap.String("step", step.Name),
			zap.String("result", result.Result.String()),
			zap.Int("return_code", result.ReturnCode),
			zap.String("result_message", result.Message),
			zap.String("notify_status", result.NotifyStatus),
			zap.Duration("duration", runTime),
		)

		if len(*result.Context) != 0 {
			// Pass the context to the next step
			job.Context = result.Context
		} else {
			stepLogger.Warn("Step result context is empty")
		}

		if jobCTX := *job.Context; jobCTX != nil {
			// append to the step result list
			rs := actions.ActionResult{
				Result:     result.Result,
				ReturnCode: result.ReturnCode,
				Message:    result.Message,
				Name:       result.Name,
				Duration:   duration.Duration{Duration: runTime},
			}
			results, err := DecodeStepResults(jobCTX)
			if err != nil {
				stepLogger.Error("Could not decode step run result", zap.Error(err))
			} else {
				jobCTX["step_run_results"] = append(results, rs)
				stepLogger.Debug("Step run results", zap.Any("results", jobCTX["step_run_results"]))
			}
		}

		if result.NotifyStatus != "" {
			retval.NotifyStatus = result.NotifyStatus
			retval.NotifyMessage = result.Message
		} else if result.Result == actions.ACTION_ABORT {
			// aborted steps are not used for workflow control and should result in a failed job
			retval.NotifyStatus = pwe.JOB_FAILURE.String()
			retval.NotifyMessage = result.Message
		}

		next = NextStep(step, result.Result)
		retval.Message = result.Message
		if next == "" {
			switch result.Result {
			case actions.ACTION_SUCCESS:
				retval.Status = pwe.JOB_SUCCESS
			case actions.ACTION_FAIL:
				retval.Status = pwe.JOB_FAILURE
			case actions.ACTION_ABORT:
				retval.Status = pwe.JOB_FAILURE
			}
			break
		}

		stepLogger = stepLogger.With(zap.String("result_message", result.Message))
		stepLogger.Debug("results processed", zap.Any("result", result))
		switch result.Result {
		case actions.ACTION_FAIL:
			stepLogger.Warn("Step Failed")
		case actions.ACTION_ABORT:
			stepLogger.Warn("Step Aborted")
		case actions.ACTION_SUCCESS:
			stepLogger.Info("Step Success")
		}

		// stop the job execution after the first failure is encountered
		// useful for investigating failing workflows
		// we'd want to reset it every time though, since we want to job to run to completion eventually
		if result.Result != actions.ACTION_SUCCESS && job.StopOnFailure {
			stepLogger.Warn("Stopping workflow since 'stop on failure' flag was set")
			retval.Status = pwe.JOB_UNDER_INVESTIGATION
			job.StopOnFailure = false
			break
		}
	}

	// no more workflow steps left
	job.CurrentStep = pointer.String(fmt.Sprintf("job:complete:%s", job.Status.String()))
	return retval
}
