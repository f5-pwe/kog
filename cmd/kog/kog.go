package main

import (
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/f5-pwe/kog/v2"
)

const (
	gitlabProject = "5998824"
)

var (
	pkgName = "kog"
	version = "v0.0.0"
	commit  = "local"
	cmdRoot = &cobra.Command{
		Use:   pkgName,
		Short: "PWE Workflow Executor",
	}
	debug            = false
	notifierHooks    *kog.NotifyHooks
	binPath, binName string
)

func main() {
	binPath, _ = os.Executable()
	binName = fmt.Sprintf("%s-%s", runtime.GOOS, runtime.GOARCH)

	// Server Config domain, this should NEVER leave the main package
	config := setupConfig()

	cmdRoot.PersistentFlags().BoolVarP(
		&debug,
		"debug",
		"",
		debug,
		``)
	config.BindPFlag("debug", cmdRoot.PersistentFlags().Lookup("debug"))

	cmdRoot.PersistentFlags().StringSliceP(
		"notifier",
		"n",
		[]string{},
		`add notifier hook (url to post to)`)
	config.BindPFlag("notifiers", cmdRoot.PersistentFlags().Lookup("notifier"))

	cmdRoot.PersistentFlags().StringP(
		"log-formatter",
		"F",
		"logfmt",
		`Select the formatter for log records ("logfmt" or "json")`)
	config.BindPFlag("logger.formatter", cmdRoot.PersistentFlags().Lookup("log-formatter"))

	cmdRoot.PersistentFlags().StringP(
		"log-level",
		"L",
		"info",
		`Set the logging level ("debug", "info", "warn", "error", "none")`)
	config.BindPFlag("logger.level", cmdRoot.PersistentFlags().Lookup("log-level"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger",
		"R",
		"",
		`Remote logger ("syslog")`,
	)
	config.BindPFlag("logger.remote", cmdRoot.PersistentFlags().Lookup("remote-logger"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger-proto",
		"",
		"udp",
		`syslog protocol`,
	)
	config.BindPFlag("logger.proto", cmdRoot.PersistentFlags().Lookup("remote-logger-proto"))

	cmdRoot.PersistentFlags().StringP(
		"remote-logger-addr",
		"",
		"127.0.0.1:514",
		`syslog addr`,
	)
	config.BindPFlag("logger.addr", cmdRoot.PersistentFlags().Lookup("remote-logger-addr"))

	cmdRoot.PersistentPreRun = func(c *cobra.Command, args []string) {
		ctx, logger := setupLogger(c.Context(), config)
		c.SetContext(ctx)
		notifierHooks = setupNotifyHooks(c.Context(), config)
		logger.Debug("kog PersistentPreRun called")
	}

	if err := startCLI(config); err != nil {
		os.Exit(127)
	}
}

func startCLI(config *viper.Viper) (err error) {
	var cmds []*cobra.Command
	cmds = append(cmds,
		setupRunCmd(config),
		setupVersionCmd(config),
		setupValidateCmd(config),
		setupUpgradeCmd(config),
	)
	cmdRoot.AddCommand(cmds...)

	// Run!
	return cmdRoot.Execute()
}
