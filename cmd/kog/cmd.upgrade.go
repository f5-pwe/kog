package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/Masterminds/semver/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/f5-pwe/kog/v2"
	"go.uber.org/zap"
)

var (
	listVersions  bool
	switchVersion string
	tmpBin        string
	minVersion    = ">= 2.0.0"
	versions      = make(semver.Collection, 0)
	gitlabClient  *gitlab.Client
	releaseMap    = make(map[string]*gitlab.Release)
)

func setupUpgradeCmd(_ *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:     "switch-to",
		Short:   "switch-to",
		Aliases: []string{"upgrade"},
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		var err error
		cmd.Parent().PersistentPreRun(c, args)
		ctx, logger := kog.ContextLogger(c.Context(), pkgName)
		c.SetContext(ctx)
		logger.Debug("upgrade PersistentPreRun called")
		gitlabClient, err = gitlab.NewClient("", gitlab.WithBaseURL("https://gitlab.com/api/v4"))
		if err != nil {
			logger.Error("failed to create gitlab client", zap.Error(err))
			os.Exit(127)
		}

		releases, _, err := gitlabClient.Releases.ListReleases(gitlabProject, &gitlab.ListReleasesOptions{})
		if err != nil {
			logger.Error("failed to list releases", zap.Error(err))
			os.Exit(127)
		}

		minVer, err := semver.NewConstraint(minVersion)
		if err != nil {
			logger.Error("failed to parse minVersion", zap.Error(err))
			os.Exit(127)
		}

		for _, release := range releases {
			v, _ := semver.NewVersion(release.TagName)
			if !minVer.Check(v) || len(release.Assets.Links) == 0 {
				continue
			}
			versions = append(versions, v)
			releaseMap[v.String()] = release
			if versions.Len() == 10 {
				break
			}
		}
		_ = sort.Reverse(versions)

		if switchVersion == "latest" {
			if versions.Len() == 0 {
				logger.Error("failed to find latest version", zap.String("min-version", minVersion))
				os.Exit(127)
			}
			switchVersion = versions[0].String()
		}
		parsedVersion, _ := semver.NewVersion(switchVersion)
		switchVersion = parsedVersion.String()
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		if listVersions {
			printVersions()
			return
		}
		upgradeVersion(c.Context())
	}

	cmd.PersistentFlags().StringVarP(
		&switchVersion,
		"version",
		"v",
		"latest",
		"version to switch to")

	cmd.PersistentFlags().BoolVarP(
		&listVersions,
		"list",
		"",
		listVersions,
		"list all hosted versions")

	return
}

func printVersions() {
	for _, rversion := range versions {
		if rversion.String() == strings.TrimLeft(version, "v") {
			fmt.Println(fmt.Sprintf("> %s", rversion.String()))
		} else {

			fmt.Println(rversion.String())
		}
	}
}

func upgradeVersion(ctx context.Context) {
	ctx, logger := kog.ContextLogger(ctx, pkgName)
	logger.Info("Switching to Version", zap.String("new-version", switchVersion))

	minVer, err := semver.NewConstraint(minVersion)
	if err != nil {
		logger.Error("failed to parse minVersion", zap.Error(err))
		os.Exit(127)
	}

	if !minVer.Check(semver.MustParse(switchVersion)) {
		logger.Warn("Version lower then minimum", zap.String("new-version", switchVersion), zap.String("min-version", minVersion))
		os.Exit(127)
	}

	tmpBin = filepath.Join(filepath.Dir(binPath), switchVersion)
	if release, ok := releaseMap[switchVersion]; !ok {
		logger.Error("Version not found", zap.String("new-version", switchVersion))
		os.Exit(127)
	} else {
		for _, link := range release.Assets.Links {
			if link.Name == binName {
				logger.Debug("try upgrade", zap.String("link", link.URL), zap.String("bin", binPath))
				if err = downloadFile(ctx, link.URL); err != nil {
					logger.Error("failed to download link", zap.String("link", link.URL), zap.Error(err))
					os.Exit(127)
				}
				if err = os.Rename(tmpBin, binPath); err != nil {
					logger.Error("failed to rename binary", zap.String("bin", binPath), zap.Error(err))
					os.Exit(127)
				}
				os.Exit(0)
			}
		}
		logger.Warn("Binary not found in release", zap.String("name", binName))
		os.Exit(127)
	}
}

func downloadFile(ctx context.Context, link string) (err error) {
	var (
		out  *os.File
		resp *http.Response
	)
	_, logger := kog.ContextLogger(ctx, pkgName)

	if out, err = os.Create(tmpBin); err != nil && !os.IsExist(err) {
		return
	} else if os.IsExist(err) {
		if out, err = os.Open(tmpBin); err != nil {
			return
		}
	}
	defer out.Close()

	// nolint:gosec
	if resp, err = http.Get(link); err != nil {
		return
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}
	if _, err = io.Copy(out, resp.Body); err != nil {
		logger.Error("failed to download file", zap.String("link", link), zap.Error(err))
	}

	err = out.Chmod(0770)

	return
}
