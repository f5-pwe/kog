package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	pwe "gitlab.com/f5-pwe/common/v2"
	"gitlab.com/f5-pwe/kog/v2"
)

var (
	executorType     string
	executor         kog.Executor
	jobFile          string
	outputFile       string
	contextFile      string
	correlationId    string
	workflow         string
	jobName          string
	extraEnv         map[string]string
	job              *pwe.Job
	startTime        time.Time
	volumes          []string
	envFiles         []string
	currentEnvImport bool
	resurnCodes      bool
	ignoreEnv        []string
)

func setupRunCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "run",
		Short: "Execute a task workflow",
	}

	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		ctx, logger := kog.ContextLogger(c.Context(), pkgName)
		c.SetContext(ctx)
		logger.Debug("run PersistentPreRun called")
		startTime = time.Now()

		var (
			err    error
			reader io.Reader
			data   []byte
		)

		if reader, err = kog.FileOrURLReader(c.Context(), jobFile); err != nil {
			logger.Error("failed to open job file", zap.Error(err))
			os.Exit(127)
		}

		if data, err = io.ReadAll(reader); err != nil {
			logger.Error("failed to read job file", zap.Error(err))
			os.Exit(127)
		}

		if job, err = kog.ParseJob(data); err != nil {
			logger.Error("failed to parse job", zap.Error(err))
			os.Exit(127)
		}

		defaultExtraEnv(ctx)

		if contextFile != "" {
			if reader, err = kog.FileOrURLReader(c.Context(), contextFile); err != nil {
				logger.Error("failed to open context file", zap.Error(err))
				os.Exit(127)
			}

			if data, err = io.ReadAll(reader); err != nil {
				logger.Error("failed to read context file", zap.Error(err))
				os.Exit(127)
			}

			if ctxf, err := kog.ParseContext(data); err == nil {
				if subCtx, ok := ctxf["context"]; ok {
					ctxf = subCtx.(map[string]interface{})
				}
				for k, v := range ctxf {
					(*job.Context)[k] = v
				}
			}
		}

		if workflow != "" {
			job.CurrentWorkflow = workflow
		}

		if job.Description == "" {
			job.Description = job.Name
		}

		if jobName != "" {
			job.Name = jobName
		}

		loggerCtx := make(map[string]string, 4)
		loggerCtx["log_formatter"] = config.GetString("logger.formatter")
		loggerCtx["log_level"] = config.GetString("logger.level")
		loggerCtx["remote_logger"] = config.GetString("logger.remote")
		loggerCtx["syslog_address"] = config.GetString("logger.addr")
		(*job.Context)["logging"] = loggerCtx

		if correlationId != "" {
			(*job.Context)["correlation_id"] = correlationId

		} else if (*job.Context)["correlation_id"] == nil || (*job.Context)["correlation_id"] == "" {
			(*job.Context)["correlation_id"] = job.ID.String()
		}

		ctx, logger = kog.ContextLogger(c.Context(), pkgName,
			zap.String("description", job.Description),
			zap.String("job_name", job.Name),
			zap.String("correlation_id", (*job.Context)["correlation_id"].(string)),
		)
		c.SetContext(ctx)
		replaceLogger(logger)

		switch executorType {
		case "docker":
			if executor, err = kog.NewDockerExecutor(c.Context()); err != nil {
				logger.Error("failed to initialize docker executor", zap.Error(err))
				os.Exit(127)
			}
		case "k8s":
			if executor, err = kog.Newk8sExecutor(c.Context()); err != nil {
				logger.Error("failed to initialize k8s executor", zap.Error(err))
				os.Exit(127)
			}
		default:
			logger.Panic("unknown executor type", zap.String("type", executorType))
			os.Exit(127)
		}
	}

	cmd.Run = func(c *cobra.Command, args []string) {
		ctx, logger := kog.ContextLogger(c.Context(), pkgName)
		c.SetContext(ctx)
		logger.Info("Starting Job")

		var result kog.JobResult

		defer func() {
			if outputFile != "" {
				var (
					ctxBytes []byte
					ctxData  string
				)
				ctxBytes, _ = json.Marshal(job.Context)

				if ctxBytes != nil {
					ctxData = fmt.Sprintf(`{"context":%s}`, string(ctxBytes))
				}

				if outputFile == "-" || outputFile == "stdout" {
					fmt.Println(ctxData)
				} else {
					logger.Info("Writing", zap.String("file", outputFile))
					if err := os.WriteFile(outputFile, []byte(ctxData), 0600); err != nil {
						logger.Error("failed to write file", zap.Error(err))
					}
				}
			}
			if r := recover(); r != nil {
				if err := notifyStatus(c.Context(), job, kog.JobResult{Status: pwe.JOB_FAILURE}); err != nil {
					logger.Error("failed to notify status", zap.Error(err))
				}
				logger.Panic("recovered from panic", zap.Any("recovered", r))
				os.Exit(127)
			}
			if err := notifyStatus(c.Context(), job, result); err != nil {
				logger.Error("failed to notify status", zap.Error(err))
				os.Exit(127)
			}

			if resurnCodes && result.Status.ReturnCode() != 0 {
				os.Exit(result.Status.ReturnCode())
			}
		}()

		result = kog.ExecuteJob(c.Context(), executor, job, notifierHooks, extraEnv, volumes)

		resultLogger := logger.With(
			zap.String("status", result.Status.String()),
			zap.String("result_message", result.Message),
			zap.String("notify_status", result.NotifyStatus),
			zap.String("notify_message", result.NotifyMessage),
		).Named("result")

		switch result.Status {
		case pwe.JOB_SUCCESS:
			resultLogger.Info("Job finished")
		case pwe.JOB_FAILURE:
			resultLogger.Error("Job Failed")
		}
	}

	cmd.PersistentFlags().StringVarP(
		&executorType,
		"executor",
		"",
		"docker",
		"Executor to use [docker, k8s]")

	cmd.PersistentFlags().StringVarP(
		&jobFile,
		"file",
		"f",
		"",
		"Job File to execute")

	cmd.PersistentFlags().StringVarP(
		&workflow,
		"workflow",
		"w",
		"",
		"Workflow to execute within the Job file")

	cmd.PersistentFlags().StringToStringVarP(
		&extraEnv,
		"env",
		"e",
		nil,
		"KEY=VALUE pairs to pass to every workflow step as env vars")

	cmd.PersistentFlags().StringSliceVarP(
		&envFiles,
		"env-file",
		"",
		nil,
		"file with KEY=VALUE pairs on each line")

	cmd.PersistentFlags().BoolVarP(
		&currentEnvImport,
		"import-current-env",
		"",
		false,
		"load the current env vars")

	cmd.PersistentFlags().BoolVarP(
		&resurnCodes,
		"use-job-rc",
		"",
		true,
		"if kog should use return codes mapping to job status")

	cmd.PersistentFlags().StringSliceVarP(
		&ignoreEnv,
		"ignore-env",
		"",
		[]string{},
		"Ignore env vars  if they contain any of the args")

	cmd.PersistentFlags().StringSliceVarP(
		&volumes,
		"volume",
		"v",
		nil,
		"volumes to mount in step containers")

	cmd.PersistentFlags().StringVarP(
		&contextFile,
		"context",
		"c",
		"",
		"File to read context from")

	cmd.PersistentFlags().StringVarP(
		&correlationId,
		"correlation-id",
		"",
		"",
		"set your own correlation-id")

	cmd.PersistentFlags().StringVarP(
		&jobName,
		"job-name",
		"",
		os.Getenv("CI_JOB_NAME"),
		"set your own job name")

	cmd.PersistentFlags().StringVarP(
		&outputFile,
		"output",
		"o",
		"",
		"File to write the final context to")

	return
}

func notifyStatus(ctx context.Context, job *pwe.Job, result kog.JobResult) error {
	duration := time.Since(startTime)
	_, logger := kog.ContextLogger(ctx, pkgName)
	logger.Debug("Job Duration", zap.Duration("duration", duration))

	if job == nil {
		return nil
	}

	if (result == kog.JobResult{}) {
		result = kog.JobResult{Status: pwe.JOB_FAILURE}
	}

	if result.NotifyStatus != "" {
		status := strings.ToLower(result.NotifyStatus)
		job.Status = pwe.JobStatusFromString(status)
		logger.Debug("Job Status", zap.String("status", status))
	} else {
		job.Status = result.Status
	}

	jobCTX := *job.Context
	if jobCTX != nil {
		jobCTX["duration_seconds"] = duration.Seconds()
	}

	if err := notifierHooks.FireNotification(job); err != nil {
		logger.Error("failed to notify status", zap.Error(err))
		return fmt.Errorf("notify hook failed: %w", err)
	}

	return nil
}

func defaultExtraEnv(ctx context.Context) {
	var (
		ok     bool
		err    error
		reader io.Reader
		myEnv  map[string]string
	)
	if extraEnv == nil {
		extraEnv = make(map[string]string)
	}
	_, logger := kog.ContextLogger(ctx, pkgName)

	for _, envFile := range envFiles {
		if reader, err = kog.FileOrURLReader(ctx, envFile); err != nil {
			logger.Error("failed to read env file", zap.Error(err))
			os.Exit(127)
		}

		if myEnv, err = godotenv.Parse(reader); err != nil {
			logger.Error("failed to parse env file", zap.Error(err))
			os.Exit(127)
		}

		for k, v := range myEnv {
			if _, ok := extraEnv[k]; ok {
				continue
			}
			extraEnv[k] = v
		}
	}

	if currentEnvImport {
		var items []string
		var key, value string
	OUTER:
		for _, pair := range os.Environ() {
			items = strings.Split(pair, "=")
			key = items[0]
			if _, ok = extraEnv[key]; ok {
				// cmd --env overrides current env vars
				continue
			}

			value = items[1]
			for _, ignore := range ignoreEnv {
				if strings.Contains(key, ignore) {
					continue OUTER
				}
			}
			extraEnv[key] = value
		}

	}
}
