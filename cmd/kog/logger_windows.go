//go:build windows

package main

import (
	"fmt"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func setupRemoteLogger(lvl string, encoder zapcore.Encoder, zapConfig *zap.Config, zapCore zapcore.Core, cfg *viper.Viper) (zapcore.Core, error) {
	switch cfg.GetString("logger.remote") {
	case "syslog":
		return nil, fmt.Errorf("syslog does not work on windows")
	}

	return zapCore, nil
}
