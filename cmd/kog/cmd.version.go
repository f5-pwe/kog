package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/f5-pwe/kog/v2"
)

func setupVersionCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "version",
		Short: "display version info",
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		ctx, logger := kog.ContextLogger(c.Context(), pkgName)
		c.SetContext(ctx)
		logger.Debug("version PersistentPreRun called")
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		fmt.Println(fmt.Sprintf("%s version %s", pkgName, version))
	}

	return
}
