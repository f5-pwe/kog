package main

import (
	"io"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/f5-pwe/common/v2/variables"
	"gitlab.com/f5-pwe/kog/v2"
	"go.uber.org/zap"
)

func setupValidateCmd(config *viper.Viper) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "validate",
		Short: "validate workflow file",
	}
	cmd.PersistentPreRun = func(c *cobra.Command, args []string) {
		cmd.Parent().PersistentPreRun(c, args)
		ctx, logger := kog.ContextLogger(c.Context(), pkgName)
		c.SetContext(ctx)
		logger.Debug("validate PersistentPreRun called")
	}
	cmd.Run = func(c *cobra.Command, args []string) {
		var (
			err    error
			reader io.Reader
			data   []byte
		)
		ctx, logger := kog.ContextLogger(c.Context(), pkgName,
			zap.String("file", jobFile),
			zap.String("context", contextFile),
		)
		c.SetContext(ctx)

		if jobFile != "" {
			if reader, err = kog.FileOrURLReader(ctx, jobFile); err != nil {
				logger.Error("failed to load job file", zap.Error(err))
				os.Exit(127)
			}
			logger.Info("Validating job file")

			if data, err = io.ReadAll(reader); err != nil {
				logger.Error("failed to read job file", zap.Error(err))
				os.Exit(127)
			}

			if job, err = kog.ParseJob(data); err != nil {
				logger.Error("failed to parse job", zap.Error(err))
				os.Exit(127)
			}
		}

		if contextFile != "" {
			logger.Info("Validating Context File")
			if reader, err = kog.FileOrURLReader(ctx, contextFile); err != nil {
				logger.Error("failed to load context file", zap.Error(err))
				os.Exit(127)
			}

			if data, err = io.ReadAll(reader); err != nil {
				logger.Error("failed to read context file", zap.Error(err))
				os.Exit(127)
			}

			if ctxf, err := kog.ParseContext(data); err == nil {
				if subCtx, ok := ctxf["context"]; ok {
					ctxf = variables.Context(subCtx.(map[string]interface{}))
				}
				ctxf["validated"] = true
			}
		}
	}

	cmd.PersistentFlags().StringVarP(
		&jobFile,
		"file",
		"f",
		"",
		"Job File to execute")

	cmd.PersistentFlags().StringVarP(
		&contextFile,
		"context",
		"c",
		"",
		"File to read context from")

	return
}
