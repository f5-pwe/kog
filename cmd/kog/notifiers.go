package main

import (
	"context"
	"strings"

	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/f5-pwe/kog/v2"
)

func setupNotifyHooks(ctx context.Context, cfg *viper.Viper) (notifiers *kog.NotifyHooks) {
	notifiers = &kog.NotifyHooks{}
	_, logger := kog.ContextLogger(ctx, pkgName)

	for _, hook := range cfg.GetStringSlice("notifiers") {
		if strings.HasPrefix(hook, "http") {
			notifiers.AddNotification(kog.NewHTTPNotifyHook(ctx, hook))
			logger.Debug("Added http notify hook", zap.String("notifier", hook))
		} else if hook == "console" {
			notifiers.AddNotification(kog.NewConsoleHook())
			logger.Debug("Added console notify hook")
		} else {
			logger.Warn("Ignoring unknown notifier", zap.String("notifier", hook))
		}
	}

	return
}
