//go:build aix || darwin || dragonfly || freebsd || (js && wasm) || linux || nacl || netbsd || openbsd || solaris

package main

import (
	"log/syslog"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func setupRemoteLogger(lvl string, encoder zapcore.Encoder, zapConfig *zap.Config, zapCore zapcore.Core, cfg *viper.Viper) (zapcore.Core, error) {
	switch cfg.GetString("logger.remote") {
	case "syslog":
		syslogLvl := syslog.LOG_EMERG
		switch lvl {
		case "error":
			syslogLvl = syslog.LOG_ERR
		case "warn":
			syslogLvl = syslog.LOG_WARNING
		case "info":
			syslogLvl = syslog.LOG_INFO
		case "debug":
			syslogLvl = syslog.LOG_DEBUG
		}
		w, err := syslog.Dial(
			cfg.GetString("logger.proto"),
			cfg.GetString("logger.addr"),
			syslogLvl|syslog.LOG_USER,
			pkgName)
		if err != nil {
			return nil, err
		}

		zapCore = zapcore.NewTee(
			zapCore,
			zapcore.NewCore(
				encoder,
				zapcore.AddSync(w),
				zapConfig.Level,
			),
		)
	}

	return zapCore, nil
}
