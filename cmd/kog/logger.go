package main

import (
	"context"
	"os"
	"runtime"
	"time"

	"github.com/spf13/viper"
	zaplogfmt "github.com/sykesm/zap-logfmt"
	"gitlab.com/f5-pwe/kog/v2"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func setupLogger(startCtx context.Context, cfg *viper.Viper) (ctx context.Context, logger *zap.Logger) {
	cfg.SetDefault("logger.formatter", "json")
	cfg.SetDefault("logger.level", "info")

	lvl := cfg.GetString("logger.level")
	if cfg.GetBool("debug") {
		lvl = "debug"
	}

	zapConfig := defaultZapConfig(lvl)
	encoder := zapcore.NewJSONEncoder(zapConfig.EncoderConfig)
	panicLogger := zap.New(zapcore.NewCore(
		encoder,
		os.Stdout,
		zapcore.DebugLevel,
	)).Named(pkgName)

	var closeWritter func()
	zapWritter, closeWritter, err := zap.Open(zapConfig.OutputPaths...)
	if err != nil {
		closeWritter()
		panicLogger.Fatal("failed to open zap writer", zap.Error(err))
		os.Exit(127)
	}
	zapErrWriter, closeWritter, err := zap.Open(zapConfig.ErrorOutputPaths...)
	if err != nil {
		closeWritter()
		panicLogger.Fatal("failed to open zap writer", zap.Error(err))
		os.Exit(127)
	}
	opts := []zap.Option{
		zap.ErrorOutput(zapErrWriter),
		zap.AddStacktrace(zap.WarnLevel),
	}
	switch cfg.GetString("logger.formatter") {
	case "json":
		encoder = zapcore.NewJSONEncoder(zapConfig.EncoderConfig)
	case "logfmt":
		encoder = zaplogfmt.NewEncoder(zapConfig.EncoderConfig)
	default:
		panicLogger.Panic("Unknown Log Formatter", zap.String("logger.formatter", cfg.GetString("logger.formatter")))
		os.Exit(127)
	}

	if zapConfig.Sampling != nil {
		opts = append(opts, zap.WrapCore(func(core zapcore.Core) zapcore.Core {
			var samplerOpts []zapcore.SamplerOption
			if zapConfig.Sampling.Hook != nil {
				samplerOpts = append(samplerOpts, zapcore.SamplerHook(zapConfig.Sampling.Hook))
			}
			return zapcore.NewSamplerWithOptions(
				core,
				time.Second,
				zapConfig.Sampling.Initial,
				zapConfig.Sampling.Thereafter,
				samplerOpts...,
			)
		}))
	}

	zapCore := zapcore.NewCore(
		encoder,
		zapWritter,
		zapConfig.Level,
	)

	zapCore, err = setupRemoteLogger(lvl, encoder, zapConfig, zapCore, cfg)
	if err != nil {
		panicLogger.Panic("Failed to connect to syslog", zap.String("logger.addr", cfg.GetString("logger.addr")), zap.Error(err))
		os.Exit(127)
	}

	logger = zap.New(zapCore, opts...).Named(pkgName)
	replaceLogger(logger)
	ctx = context.WithValue(startCtx, kog.LoggerContextKey, logger)

	return ctx, logger
}

func replaceLogger(logger *zap.Logger) {
	zap.RedirectStdLog(logger)
	zap.ReplaceGlobals(logger)
}

func defaultZapConfig(level string) *zap.Config {
	hostname, _ := os.Hostname()
	lvl, err := zap.ParseAtomicLevel(level)
	if err != nil {
		lvl = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	}
	return &zap.Config{
		Level:             lvl,
		OutputPaths:       []string{"stdout"},
		ErrorOutputPaths:  []string{"stderr"},
		DisableCaller:     false,
		DisableStacktrace: false,
		InitialFields: map[string]interface{}{
			"os":         runtime.GOOS,
			"arch":       runtime.GOARCH,
			"runtime":    runtime.Version(),
			"process_id": os.Getpid(),
			"hostname":   hostname,
		},
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		EncoderConfig: zapcore.EncoderConfig{
			CallerKey:      "caller",
			LevelKey:       "severity",
			LineEnding:     "\n",
			MessageKey:     "message",
			NameKey:        "logger",
			StacktraceKey:  "stacktrace",
			TimeKey:        "timestamp",
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeDuration: zapcore.NanosDurationEncoder,
			EncodeTime:     zapcore.RFC3339NanoTimeEncoder,
		},
	}
}
